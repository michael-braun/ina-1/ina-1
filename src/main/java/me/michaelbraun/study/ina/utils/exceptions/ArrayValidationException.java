package me.michaelbraun.study.ina.utils.exceptions;

public class ArrayValidationException extends ValidationException {
	private final int index;

	public ArrayValidationException(String modelName, String variableName, int index) {
		super(
			modelName,
			variableName,
			"Invalid value of variable " + variableName + " on index " + index + " inside model " + modelName
		);

		this.index = index;
	}

	public int getIndex() {
		return index;
	}
}
