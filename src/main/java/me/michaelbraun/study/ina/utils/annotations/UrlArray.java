package me.michaelbraun.study.ina.utils.annotations;

import javax.validation.Constraint;

@Constraint(validatedBy = UrlArrayValidator.class)
public @interface UrlArray {
}
