package me.michaelbraun.study.ina.utils;

import me.michaelbraun.study.ina.models.rss.RssChannel;
import me.michaelbraun.study.ina.models.rss.RssItem;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * A helper-class to read a rss-file
 */
public class RssReader {
	private final String url;

	/**
	 * Creates a new reader based on a url
	 * @param url url to the rss-feed
	 */
	public RssReader(String url) {
		this.url = url;
	}

	/**
	 * Executes the reading of the file
	 * @return A RssChannel that describes the rss-feed
	 * @throws IOException
	 */
	public RssChannel execute() throws IOException {
		Document doc = Jsoup.connect(url).get();
		Element channel = doc.select("channel").first();
		Elements channelItems = doc.select("channel item");

		List<RssItem> retVal = new ArrayList<>();

		for (Element e : channelItems) {
			String title = e.getElementsByTag("title").first().text();
			String url = e.getElementsByTag("link").first().text();
			Element descriptionTag = e.getElementsByTag("description").first();
			String description = (descriptionTag != null) ? descriptionTag.text() : null;

			retVal.add(new RssItem(title, url, description));
		}

		String channelTitle = channel.getElementsByTag("title").first().text();

		return new RssChannel(channelTitle, retVal.toArray(new RssItem[0]));
	}
}
