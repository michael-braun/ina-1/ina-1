package me.michaelbraun.study.ina.utils;

import me.michaelbraun.study.ina.utils.exceptions.FeedViolationException;
import me.michaelbraun.study.ina.utils.exceptions.ValidationException;

/**
 * Abstract class that should be used as base-class on all Helper-classes
 * @param <T>
 */
public abstract class HelperBase<T> {
	private T data;

	private boolean invalid = false;

	/**
	 * Gets the (validated) data/bean
	 * @return
	 */
	public T getData() {
		if (invalid) return null;

		return data;
	}

	/**
	 * Gets invalid data/bean
	 * @return
	 */
	public T getInvalidData() {
		if (!invalid) return null;

		return data;
	}

	/**
	 * Checks whether the contained data is invalid
	 * @return
	 */
	public boolean isInvalid() {
		return invalid;
	}

	/**
	 * Sets the new data/bean
	 * @param data The bean/data that should be set
	 * @throws FeedViolationException
	 * @throws ValidationException
	 */
	public void setData(T data) throws FeedViolationException, ValidationException {
		this.setData(data, true);
	}

	/**
	 * Sets the new data/bean but allows you to set the valid-flag with the data
	 * @param data The bean/data that should be set
	 * @param valid Whether the given data is valid or not
	 */
	public void setData(T data, boolean valid) {
		this.invalid = !valid;
		this.data = data;
	}
}
