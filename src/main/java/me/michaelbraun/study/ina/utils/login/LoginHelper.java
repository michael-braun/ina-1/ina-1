package me.michaelbraun.study.ina.utils.login;

import me.michaelbraun.study.ina.models.login.LoginBean;
import me.michaelbraun.study.ina.utils.HelperBase;
import me.michaelbraun.study.ina.utils.PersistenceUtil;
import me.michaelbraun.study.ina.utils.ValidationUtil;
import me.michaelbraun.study.ina.utils.exceptions.FeedViolationException;
import me.michaelbraun.study.ina.utils.exceptions.ValidationException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import java.io.IOException;
import java.util.Set;

public class LoginHelper extends HelperBase<LoginBean> {
	public static LoginBean RequestToBean(HttpServletRequest request) {
		String username = request.getParameter("username");
		String password = request.getParameter("password");

		LoginBean loginBean = new LoginBean();
		loginBean.setUsername(username);
		loginBean.setPassword(password);

		return loginBean;
	}

	public static boolean RedirectAuth(HttpServletRequest request, HttpServletResponse response) throws IOException {
		if (request.getSession().getAttribute("login") == null) {
			response.sendRedirect(request.getContextPath() + "/login");
			return true;
		}

		return false;
	}

	public static LoginHelper FromSession(HttpServletRequest request) {
		return ((LoginHelper) request.getSession().getAttribute("login"));
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			LoginBean tmpLoginBean = LoginHelper.RequestToBean(request);
			ValidationUtil<LoginBean> validator = new ValidationUtil<>();
			if(!validator.isValid(tmpLoginBean)) {
				Set<ConstraintViolation<LoginBean>> violations = validator.getViolations();

				StringBuilder builder = new StringBuilder();

				for (ConstraintViolation<LoginBean> violation : violations) {
					builder.append('[');
					builder.append(violation.getPropertyPath());
					builder.append("] ");
					builder.append(violation.getInvalidValue());
					builder.append(": ");
					builder.append(violation.getMessage());
					builder.append("<br />");
					builder.append('\n');
				}

				request.setAttribute("error", builder.toString());
				RequestDispatcher requestDispatcher = request.getRequestDispatcher("/views/login");
				requestDispatcher.forward(request, response);

				return;
			}

			LoginBean loginBean = PersistenceUtil.obtainWhere(LoginBean.class, "username", String.class, tmpLoginBean.getUsername());

			if (loginBean != null) {
				if (!loginBean.getPassword().equals(tmpLoginBean.getPassword())) {
					request.setAttribute("error", "Benutzername/Password falsch!");
					RequestDispatcher requestDispatcher = request.getRequestDispatcher("/views/login");
					requestDispatcher.forward(request, response);
				}

				this.setData(loginBean);
			} else {
				this.setData(tmpLoginBean);
				this.save();
			}
		} catch (ValidationException | FeedViolationException e) {
			e.printStackTrace();
		}

		request.getSession().setAttribute("login", this);
		response.sendRedirect(request.getContextPath());
	}

	public boolean save() {
		if (this.isInvalid()) {
			return false;
		}

		PersistenceUtil.saveOrUpdate(this.getData());
		return true;
	}

	public void doLogout(HttpServletRequest request, HttpServletResponse response) throws IOException {
		request.getSession().removeAttribute("login");
		response.sendRedirect(request.getContextPath() + "/login");
	}
}
