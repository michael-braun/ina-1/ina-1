package me.michaelbraun.study.ina.utils.exceptions;

public class ValidationException extends Exception {
	private final String modelName;
	private final String variableName;

	public ValidationException(String modelName, String variableName) {
		this(modelName, variableName, "Invalid value of variable " + variableName + " inside model " + modelName);
	}

	public ValidationException(String modelName, String variableName, String message) {
		super(message);

		this.modelName = modelName;
		this.variableName = variableName;
	}

	public String getModelName() {
		return modelName;
	}

	public String getVariableName() {
		return variableName;
	}
}
