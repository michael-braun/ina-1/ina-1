package me.michaelbraun.study.ina.utils.input;

import me.michaelbraun.study.ina.models.input.FeedBean;
import me.michaelbraun.study.ina.models.login.LoginBean;
import me.michaelbraun.study.ina.models.rss.RssChannel;
import me.michaelbraun.study.ina.utils.*;
import me.michaelbraun.study.ina.utils.exceptions.ArrayValidationException;
import me.michaelbraun.study.ina.utils.exceptions.FeedViolationException;
import me.michaelbraun.study.ina.utils.exceptions.ValidationException;
import me.michaelbraun.study.ina.utils.login.LoginHelper;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class FeedHelper extends HelperBase<FeedBean> {
	public static FeedBean RequestToBean(HttpServletRequest request) {
		String name = request.getParameter("name");
		String[] url = request.getParameterValues("url[]");
		String[] cleanedUrls = Arrays.stream(url).filter(x -> !x.equals("")).toArray(String[]::new);

		FeedBean feedBean = new FeedBean();
		feedBean.setName(name);
		feedBean.setUrl(cleanedUrls);
		feedBean.setUser(((LoginHelper) request.getSession().getAttribute("login")).getData());

		return feedBean;
	}

	public static FeedBean GetBean(long id) {
		List<FeedBean> beans = PersistenceUtil.obtainAll(FeedBean.class, "id", Long.class, id);

		if (beans == null || beans.size() == 0) {
			return null;
		}

		return beans.get(0);
	}

	@Override
	public void setData(FeedBean data) throws FeedViolationException, ValidationException {
		if (data != null) {
			// this.validateData(data);
			ValidationUtil<FeedBean> validator = new ValidationUtil<>();
			if (!validator.isValid(data)) {
				Set<ConstraintViolation<FeedBean>> violations = validator.getViolations();
				throw new FeedViolationException(violations);
			}
		}

		super.setData(data);
	}

	private void validateData(FeedBean data) throws ArrayValidationException {
		String[] urls = data.getUrl();

		for (int i = 0; i < urls.length; i++) {
			if (!UrlHelper.validateUrl(UrlHelper.constructUrl(urls[i]))) {
				throw new ArrayValidationException("FeedBean", "url", i);
			}
		}
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (LoginHelper.RedirectAuth(request, response)) return;

		LoginHelper loginHelper = LoginHelper.FromSession(request);

		try {
			FeedBean feedBean = FeedHelper.GetBean(loginHelper.getData());
			this.setData(feedBean);
		} catch (ValidationException | FeedViolationException e) {
			e.printStackTrace();
		}

		request.setAttribute("input", this);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/views/input");
		requestDispatcher.forward(request, response);
	}

	private static FeedBean GetBean(LoginBean data) {
		List<FeedBean> beans = PersistenceUtil.obtainAll(FeedBean.class, "user_id", Long.class, data.getId());

		if (beans == null || beans.size() == 0) {
			return null;
		}

		return beans.get(0);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (LoginHelper.RedirectAuth(request, response)) return;

		try {
			FeedBean feedBean = FeedHelper.RequestToBean(request);
			this.setData(feedBean);
			this.save();

			RssChannel[] channels = FeedHelper.fetchChannels(feedBean);
			feedBean.setChannels(channels);
			this.setData(feedBean);
		} catch (FeedViolationException e) {
			request.setAttribute("input", this);
			request.setAttribute("error", e.toString());

			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/views/input");
			requestDispatcher.forward(request, response);
			return;
		} catch (ValidationException e) {
			e.printStackTrace();
		}

		request.setAttribute("input", this);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/views/output");
		requestDispatcher.forward(request, response);
	}

	private static RssChannel[] fetchChannels(FeedBean feedBean) {
		try {
			List<RssChannel> channels = new ArrayList<>();
			for (String url : feedBean.getUrl()) {
				String requestUrl = UrlHelper.constructUrl(url);
				String[] rssUrls = RssResolver.resolve(requestUrl);

				if (rssUrls.length < 1) {
					continue;
				}

				RssReader rssReader = new RssReader(rssUrls[0]);
				RssChannel channel = rssReader.execute();
				channels.add(channel);
			}
			return channels.toArray(new RssChannel[0]);
		} catch (IOException ex) {
			ex.printStackTrace();
			return new RssChannel[0];
		}
	}

	public boolean save() {
		if (this.isInvalid()) {
			return false;
		}

		LoginBean loginBean = this.getData().getUser();
		FeedBean oldFeedBean = FeedHelper.GetBean(loginBean);

		if (oldFeedBean != null) {
			FeedBean currentFeedBean = this.getData();
			currentFeedBean.setId(oldFeedBean.getId());
			try {
				super.setData(currentFeedBean); // unnecessary, but more clear
			} catch(ValidationException | FeedViolationException ex) {
				ex.printStackTrace();
			}
		}

		PersistenceUtil.saveOrUpdate(this.getData());
		return true;
	}
}
