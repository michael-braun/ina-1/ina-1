package me.michaelbraun.study.ina.utils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Utility-class that could get a rss feed based on a website (when supported)
 */
public class RssResolver {
	/**
	 * Resolves all rss feeds that are set inside the website
	 * @param url the website that should be checked
	 * @return all rss-feeds from the site
	 * @throws IOException
	 */
	public static String[] resolve(String url) throws IOException {
		Document doc = Jsoup.connect(url).get();

		Elements links = doc.select("link[type=application/rss+xml]");

		List<String> retVal = new ArrayList<String>();

		for(int i = 0, len = links.size(); i < len; i++) {
			String rssUrl = links.get(i).attr("href");

			rssUrl = UrlHelper.resolve(url, rssUrl);

			if (UrlHelper.validateUrl(rssUrl)) {
				retVal.add(rssUrl);
			}
		}

		return retVal.toArray(new String[0]);
	}
}
