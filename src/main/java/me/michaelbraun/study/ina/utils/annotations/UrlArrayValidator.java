package me.michaelbraun.study.ina.utils.annotations;

import me.michaelbraun.study.ina.utils.UrlHelper;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UrlArrayValidator implements ConstraintValidator<UrlArray, String[]> {
	@Override
	public void initialize(UrlArray constraintAnnotation) {	}

	@Override
	public boolean isValid(String[] value, ConstraintValidatorContext context) {
		for (String data : value) {
			if (!UrlHelper.validateUrl(UrlHelper.constructUrl(data))) {
				return false;
			}
		}

		return true;
	}
}
