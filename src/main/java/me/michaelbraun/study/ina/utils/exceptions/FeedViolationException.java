package me.michaelbraun.study.ina.utils.exceptions;

import me.michaelbraun.study.ina.models.input.FeedBean;

import javax.validation.ConstraintViolation;
import java.util.Set;

public class FeedViolationException extends Throwable {
	private Set<ConstraintViolation<FeedBean>> violations;

	public FeedViolationException(Set<ConstraintViolation<FeedBean>> violations) {
		this.violations = violations;
	}

	public Set<ConstraintViolation<FeedBean>> getViolations() {
		return violations;
	}

	public String toString() {
		StringBuilder builder = new StringBuilder();

		for (ConstraintViolation<FeedBean> violation : violations) {
			builder.append('[');
			builder.append(violation.getPropertyPath());
			builder.append("] ");
			builder.append(violation.getInvalidValue());
			builder.append(": ");
			builder.append(violation.getMessage());
			builder.append("<br />");
			builder.append('\n');
		}

		return builder.toString();
	}
}
