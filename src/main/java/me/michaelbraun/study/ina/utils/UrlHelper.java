package me.michaelbraun.study.ina.utils;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * Utility class for managing urls
 */
public class UrlHelper {
	/**
	 * Creates a http-url based on a string
	 * @param url url that the user has set
	 * @return "valid" http url
	 */
	public static String constructUrl(String url) {
		if (!url.contains(".")
				|| url.startsWith("http://")
				|| url.startsWith("https://")) {
			return url;
		}

		return "http://" + url;
	}

	/**
	 * Validates an url
	 * @param url the url that should be validated
	 * @return whether the url is valid or not
	 */
	public static boolean validateUrl(String url) {
		try {
			new URL(url).toURI();
			return true;
		}
		catch (URISyntaxException | MalformedURLException exception) {
			return false;
		}
	}

	/**
	 * Resolves an absolute path (without the host-part)
	 * @param baseUrl host part of the url
	 * @param url url that was given by the site (full url or path)
	 * @return full url (hostname + path)
	 */
	public static String resolve(String baseUrl, String url) {
		if (!url.startsWith("/")) {
			return url;
		}

		return baseUrl + url;
	}
}
