package me.michaelbraun.study.ina;

import me.michaelbraun.study.ina.utils.RssReader;
import me.michaelbraun.study.ina.utils.RssResolver;
import me.michaelbraun.study.ina.utils.UrlHelper;

import java.util.Arrays;

public class Main {
	public static void main(String... args) {
		if (args.length != 1) {
			System.out.println("Invalid argument count");
			return;
		}

		String url = UrlHelper.constructUrl(args[0]);

		try {
			String[] data = RssResolver.resolve(url);
			System.out.println(Arrays.toString(data));
			RssReader rssReader = new RssReader(data[0]);
			System.out.println(rssReader.execute());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
