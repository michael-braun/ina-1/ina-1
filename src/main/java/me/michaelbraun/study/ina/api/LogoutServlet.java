package me.michaelbraun.study.ina.api;

import me.michaelbraun.study.ina.utils.login.LoginHelper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LogoutServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request,
						 HttpServletResponse response) throws IOException
	{
		LoginHelper loginHelper = new LoginHelper();
		loginHelper.doLogout(request, response);
	}
}
