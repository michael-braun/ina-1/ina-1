package me.michaelbraun.study.ina.api;

import me.michaelbraun.study.ina.utils.input.FeedHelper;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BeanServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request,
						  HttpServletResponse response) throws ServletException, IOException
	{
		FeedHelper feedHelper = new FeedHelper();
		feedHelper.doGet(request, response);
	}

	protected void doPost(HttpServletRequest request,
						 HttpServletResponse response) throws ServletException, IOException
	{
		FeedHelper feedHelper = new FeedHelper();
		feedHelper.doPost(request, response);
	}
}