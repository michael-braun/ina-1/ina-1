package me.michaelbraun.study.ina.models.login;

import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
public class LoginBean {
	private long id;

	private String username;

	private String password;

	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(unique = true)
	@NotNull
	@Pattern(regexp="^[a-zA-Z]\\S*$", message="should not be empty and start with a letter")
	@Length(min = 3, message = "The field must be at least 3 characters")
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@NotNull
	@Pattern(regexp=".*\\S.*", message="should not be empty")
	@Length(min = 8, message = "The field must be at least 8 characters")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
