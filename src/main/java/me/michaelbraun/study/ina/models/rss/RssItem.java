package me.michaelbraun.study.ina.models.rss;

/**
 * Class to describe a single RssItem
 */
public class RssItem {
	private final String title;
	private final String url;
	private final String description;

	/**
	 * Constructs a RssItem
	 * @param title the title of the feed-item
	 * @param url an url to the connected news-entry on the website
	 * @param description Description of the RssItem
	 */
	public RssItem(String title, String url, String description) {
		this.title = title;
		this.url = url;
		this.description = description;
	}

	/**
	 * Get the title of the RssItem
	 * @return title of the item
	 */
	public String getTitle() {
		return this.title;
	}

	/**
	 * Get the url to the website of this item
	 * @return url to the news
	 */
	public String getUrl() {
		return this.url;
	}

	/**
	 * Get the description of this item
	 * @return description of the item
	 */
	public String getDescription() {
		return this.description;
	}

	@Override
	public String toString() {
		return "RssItem{" +
				"title='" + title + '\'' +
				", url='" + url + '\'' +
				'}';
	}
}
