package me.michaelbraun.study.ina.models.input;

import me.michaelbraun.study.ina.models.login.LoginBean;
import me.michaelbraun.study.ina.models.rss.RssChannel;
import me.michaelbraun.study.ina.utils.annotations.UrlArray;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
public class FeedBean {
	private long id;

	private String name;

	private String[] url;

	private RssChannel[] channels;

	private LoginBean user;

	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@NotNull
	@Pattern(regexp=".*\\S.*", message="darf nicht leer sein")
	@Length(min = 5, message = "The field must be at least 5 characters")
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@UrlArray
	public String[] getUrl() {
		return url;
	}

	public String getUrl(int index) {
		return url.length > index ? url[index] : null;
	}

	@Transient
	public RssChannel[] getChannels() {
		return this.channels;
	}

	@OneToOne()
	public LoginBean getUser() {
		return user;
	}

	public void setUrl(String[] url) {
		this.url = url;
	}

	public void setUrl(int index, String url) {
		this.url[index] = url;
	}

	public void setChannels(RssChannel[] channels) {
		this.channels = channels;
	}

	public void setUser(LoginBean user) {
		this.user = user;
	}
}
