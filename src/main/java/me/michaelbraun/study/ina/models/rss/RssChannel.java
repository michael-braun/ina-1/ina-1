package me.michaelbraun.study.ina.models.rss;

import java.util.Arrays;

/**
 * Class to describe a RssChannel
 */
public class RssChannel {
	private final String title;
	private final RssItem[] items;

	/**
	 * Constructs a RssChannel
	 * @param title The title of the RssChannel
	 * @param items All items that are inside the RssChannel
	 */
	public RssChannel(String title, RssItem[] items) {
		this.title = title;
		this.items = items;
	}

	/**
	 * Get the title of a RssChannel
	 * @return title of the RssChannel
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Get all items that are inside the RssChannel
	 * @return all items of the channel
	 */
	public RssItem[] getItems() {
		return items;
	}

	@Override
	public String toString() {
		return "RssChannel{" +
				"title='" + title + '\'' +
				", items=" + Arrays.toString(items) +
				'}';
	}
}
