<%--
  Created by IntelliJ IDEA.
  User: michael
  Date: 29.05.19
  Time: 09:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="me.michaelbraun.study.ina.utils.input.FeedHelper" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%
    if(!(request.getAttribute("input") instanceof FeedHelper)) {
        response.sendRedirect(request.getContextPath() + "/");
    }

    FeedHelper feedHelper = (FeedHelper) request.getAttribute("input");
    pageContext.setAttribute("feedHelper", feedHelper);
%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1><%= feedHelper.getData().getName() %></h1>
<c:forEach items="${feedHelper.getData().getChannels()}" var="channel">
    <h2>
        <c:out value = "${channel.getTitle()}"/>
    </h2>
    <c:forEach items="${channel.getItems()}" var="item">
        <div>
            <h3>
                <a href="<c:out value="${item.getUrl()}"/>">
                    <c:out value="${item.getTitle()}"/>
                </a>
            </h3>
            <p><c:out value="${item.getDescription()}"/></p>
            <a href="<c:out value="${item.getUrl()}"/>">Weiterlesen ...</a>
        </div>
    </c:forEach>
</c:forEach>
</body>
</html>
