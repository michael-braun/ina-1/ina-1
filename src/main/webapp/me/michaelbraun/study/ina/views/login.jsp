<%--
  Created by IntelliJ IDEA.
  User: michael
  Date: 29.05.19
  Time: 11:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Login</title>
</head>
<body>
<form action="<%= request.getContextPath() %>/login" method="post">
    <input
        type="text"
        name="username"
        placeholder="Benutzername"
    />
    <br />
    <input
            type="password"
            name="password"
            placeholder="Kennwort"
    />
    <br />
    <br />
    <c:if test="${not empty error}">
        <div>
            <%= request.getAttribute("error") %>
        </div>
        <br />
    </c:if>
    <input type="submit" value="Senden" />
</form>
</body>
</html>
