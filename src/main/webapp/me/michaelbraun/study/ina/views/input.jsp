<%--
  Created by IntelliJ IDEA.
  User: michael
  Date: 29.05.19
  Time: 11:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="me.michaelbraun.study.ina.utils.input.FeedHelper" %>
<%@ page import="me.michaelbraun.study.ina.models.input.FeedBean" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%
    FeedHelper feedHelper = null;
    FeedBean feedBean = null;

    if(request.getAttribute("input") instanceof FeedHelper) {
        feedHelper = (FeedHelper) request.getAttribute("input");
        feedBean = feedHelper.isInvalid() ? feedHelper.getInvalidData() : feedHelper.getData();
    }

    boolean hasFeedBean = (feedBean != null);
    pageContext.setAttribute("feedBean", feedBean);
    pageContext.setAttribute("hasFeedBean", hasFeedBean);
%>
<html>
<head>
    <title>Eingabe</title>
    <style>
        input {
            display: block;
        }
    </style>
</head>
<body>
<form action="<%= request.getContextPath() %>/check" method="post">
    <c:if test="${not empty error}">
    <div>
        <%= request.getAttribute("error") %>
    </div>
    </c:if>
    <label>
        Name:
        <input
            type="text"
            name="name"
            value="<%= hasFeedBean ? feedBean.getName() : "" %>"
        />
    </label>
    <div id="input-list">
        <c:forEach items="${feedBean.getUrl()}" var="url" varStatus="loop">
            <input
                type="text"
                name="url[]"
                value="<c:out value="${url}" />"
                data-id="<c:out value="${loop.index}" />"
                placeholder="url"
            />
        </c:forEach>
        <input
            type="text"
            name="url[]"
            value=""
            data-id="<c:out value="${hasFeedBean ? fn:length(feedBean.getUrl()) : 0}" />"
            placeholder="url"
        />
    </div>
    <input type="submit" value="Speichern" />
</form>
<form action="<%= request.getContextPath() %>/logout" method="get">
    <input type="submit" value="Logout" />
</form>
<script>
    const inputList = document.getElementById('input-list');
    const inputs = Array.from(document.querySelectorAll('input[name="url[]"]'));
    const template = inputs[inputs.length - 1].outerHTML;
    const drawBoard = document.createElement('div');
    drawBoard.style.display = 'none';
    document.body.appendChild(drawBoard);

    function onBlur(event) {
        const target = event.target;
        const targetId = parseInt(target.dataset.id, 10);

        if (target.value === '' && targetId !== inputs.length - 1) {
            removeInput(target);
        }

        if (target.value !== '' && targetId === inputs.length - 1) {
            createInput();
        }
    }

    function updateInputIds() {
        inputs.forEach((input, index) => {
            input.dataset.id = index;
        });
    }

    function createInput() {
        window.requestAnimationFrame(() => {
            drawBoard.innerHTML = template;
            const inputElement = drawBoard.children[drawBoard.children.length - 1];
            inputElement.addEventListener('blur', onBlur);
            inputList.appendChild(inputElement);
            inputs.push(inputElement);

            updateInputIds();
        });
    }

    function removeInput(target) {
        const targetId = parseInt(target.dataset.id, 10);

        inputList.removeChild(target);
        inputs.splice(targetId, 1);
        updateInputIds();
    }

    inputs.forEach((input) => {
        input.addEventListener('blur', onBlur);
    });
</script>
</body>
</html>
