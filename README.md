# Praktikum 6: Hibernate/Validation
__Michael Braun__

## Allgemein
Der Tomcat-Server wurde nicht "klassisch" installiert, sondern das Tomcat-Docker-Image benutzt.

Hierdurch ist eine Integration in die IDE (hier IntelliJ) nicht möglich, 
was allerdings zunächst nicht weiter schlimm ist, da der Build mit gradle
ausgeführt wird und die war-Datei direkt in den Docker-Container
injected (mittels Volume-Mount eingebunden) wird.

Debugging ist über __JPDA__ (Port 8001) möglich.

Die veränderte Konfiguration wird automatisch über eine docker-compose
Datei angewendet.

Der Tomcat-Server lässt sich aus dem Hauptverzeichnis mit
```docker-compose -f ./config/docker/docker-compose.yml up```
starten

## Teil 1:

Für die Validierung des Kennwortes wurde die RegExp aus der Vorlesungsfolie genommen:
```.*\S.*```. Außerdem wurde eine Längenprüfung hinzugefügt (wäre auch über die RegExp gegangen, es
wurde aber eine eigene Annotation hierfür benutzt).

Für die Validierung des Benutzernamens wurde die RegExp ```^[a-zA-Z]\S*$``` erstellt. Diese
stellt sicher, dass der Benutzername immer mit einem Buchstaben anfängt (viele Systeme mögen es nicht, wenn ein
Benutzername mit Zahlen oder Sonderzeichen startet).

Für die URL war zunächst eine eigene Annotation/ein eigener Validator geplant. Leider wird dieser allerdings nicht aufgerufen.
Der Code hierfür wurde noch im Projekt gelassen.

## Teil 2:

Die eigenen Validierungen wurden durch Validierungen mittels Hibernate ersetzt. Basierend auf dem Code aus der Vorlesung (zur Generierung
der Fehlermeldung) wurde ein eigener Code implementiert, welcher einen StringBuilder und nicht den Response-Stream benutzt, 
sowie zusätzlich den Namen der Eigenschaft ausgibt.
